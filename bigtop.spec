Name: bigtop
Version: 1.5.0
Release: 1%{?dist}
Summary: is a project for the development of packaging and tests of the Apache Hadoop ecosystem

License: Apache License, Version 2.0
Source0: bigtop-%{version}.tar.gz

BuildRequires: git java-1.8.0-openjdk-devel maven
Provides: Bigtop = %{version}
Requires: java-1.8.0-openjdk

%description
The primary goal of Apache Bigtop is to build a community around the packaging and interoperability testing of Apache Hadoop-related projects. This includes testing at various levels (packaging, platform, runtime, upgrade, etc...) developed by a community with a focus on the system as a whole, rather than individual projects.

%prep
%setup -q -n %{name}-%{version}

%build

%install

%files
%defattr(-,root,root,755)

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Sun Apr 4 2020 Hao Zhang <unioah@isrc.iscas.ac.cn>
- add spec
